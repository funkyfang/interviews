<?php

class Tests {
  private $tests = array();

  /*
    Expects $tests to be in the format:
      array(
        array(
          'instance' => new SomeTest(),
          'test_args' => array(
            // Args used for test.
          ),
        ),
        ...
      )
  */
  public function __construct($tests) {
    $this->tests = $tests;
  } 

  public function run() {
    foreach ($this->tests as $test) {
      $test_instance = $test['instance'];
      $test_args = $test['test_args'];
      if (self::has_implemented_ITest($test_instance)) {
        $test_instance->run($test_args);
      }
    }
  }

  private static function has_implemented_ITest($class) {
    if ($class instanceof ITest) {
      return true;
    }
    echo get_class($class)." must implement ITest.\n";
    return false;
  }
}
