<?php

interface ITest {
  public function run($data);
  public function test($data);
}

trait Test {
  private static $pass = 'Pass';
  private static $fail = 'Fail';
}
