<?php
require_once('Deck.php');

$newDeck = new Deck();
$sortedDeck = new Deck();
$sortedDeck->shuffle();
$sortedDeck->sort();
print 'Sort ' . ($newDeck->equals($sortedDeck) ? "passed.\n" : "FAILED!\n");

$missingCardFound = true;
$missingCardFoundv2 = true;
$newDeck = new Deck();
for ($i=0; $i<count($newDeck->cards); $i++) {
  $removedCard = $newDeck->removeCardAt($i);
  $missingCard = $newDeck->findMissingCard();
  $removedCardv2 = $newDeck->removeCardAt($i);
  $missingCardv2 = $newDeck->findMissingCardv2(
                     intval(count($newDeck->fiftyOneCards)/2),
                     count($newDeck->cards));
  if (!$removedCard->equals($missingCard)) {
    $missingCardFound = false;
  }
  if (!$removedCardv2->equals($missingCardv2)) {
    $missingCardFoundv2 = false;
  }
}
print 'Find missing card ' . ($missingCardFound ? "passed.\n" : "FAILED!\n");
print 'Find missing card v2 ' . ($missingCardFoundv2 ? "passed.\n" : "FAILED!\n");
