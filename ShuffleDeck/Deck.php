<?php

abstract class CardSuits {
  const Clubs = 'Clubs';
  const Diamonds = 'Diamonds';
  const Hearts = 'Hearts';
  const Spades = 'Spades';
}

abstract class NamedCards {
  const Jack = 'Jack';
  const Queen = 'Queen';
  const King = 'King';
  const Ace = 'Ace';
}

class Card {
  public $value;
  public $suit;

  public function __construct($value, $suit) {
    $this->value = $value;
    $this->suit = $suit;
  }

  public function equals($card) {
    if (($this->value === $card->value) && ($this->suit === $card->suit)) {
      return true;
    }
    return false;
  }
}

class Deck {
  public static $orderedValues = [
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    NamedCards::Jack,
    NamedCards::Queen,
    NamedCards::King,
    NamedCards::Ace
  ];
  public static $orderedSuits = [
    CardSuits::Clubs,
    CardSuits::Diamonds,
    CardSuits::Hearts,
    CardSuits::Spades
  ];
  public $cards = [];
  public $orderedCards = [];
  public $fiftyOneCards = [];

  public function __construct() {
    foreach (self::$orderedValues as $value) {
      foreach (self::$orderedSuits as $suit) {
        $this->cards[] = new Card($value, $suit);
      }
    }
  }

  public function shuffle() {
    shuffle($this->cards);
  }

  public function equals($deck) {
    for ($i=0; $i<count($this->cards); $i++) {
      if (!$this->cards[$i]->equals($deck->cards[$i])) {
        return false;
      }
    }
    return true;
  }

  public function sort() {
    $emptyDeck = [];
    for ($i=0; $i<count(self::$orderedValues); $i++) {
      $emptyDeck[] = [];
      for ($j=0; $j<count(self::$orderedSuits); $j++) {
        $emptyDeck[$i][] = NULL;
      }
    }

    foreach ($this->cards as $card) {
      $value = $card->value;
      $suit = $card->suit;

      switch ($value) {
        case NamedCards::Jack:
        case NamedCards::Queen:
        case NamedCards::King:
        case NamedCards::Ace:
          $value = self::$orderedValues[0] + 
            array_search($value, self::$orderedValues);
          break;
      }

      $index = $value - self::$orderedValues[0];
      $emptyDeck[$index][array_search($suit, self::$orderedSuits)] = $card;
    }

    $this->cards = [];
    foreach ($emptyDeck as $cards) {
      $this->cards = array_merge($this->cards, $cards);
    }
  }

  public function removeCardAt($index) {
    $this->sort();
    $this->orderedCards = $this->cards;
    $this->fiftyOneCards = $this->cards;
    $removedCard = $this->fiftyOneCards[$index];
    unset($this->fiftyOneCards[$index]);
    $this->fiftyOneCards = array_values($this->fiftyOneCards);
    return $removedCard;
  }

  public function findMissingCard() {
    $pivot = intval(count($this->fiftyOneCards)/2);
    if ($this->orderedCards[$pivot]->equals($this->fiftyOneCards[$pivot])) {
      if ($pivot === 0) {
        return $this->orderedCards[0];
      }
      $this->fiftyOneCards = array_slice($this->fiftyOneCards, $pivot+1);
      $this->orderedCards = array_slice($this->orderedCards, $pivot+1);
      return $this->findMissingCard();
    } else {
      if ($pivot-1 < 0) {
        return $this->orderedCards[0];
      } else if ($this->orderedCards[$pivot-1]
                      ->equals($this->fiftyOneCards[$pivot-1])) {
        return $this->orderedCards[$pivot];
      } else {
        $this->fiftyOneCards = array_slice($this->fiftyOneCards, 0, $pivot);
        $this->orderedCards = array_slice($this->orderedCards, 0, $pivot);
        return $this->findMissingCard();
      }
    }
  }

  public function indexToCard($index) {
    $numSuits = count(self::$orderedSuits);
    $valueIndex = intval($index/$numSuits);
    $suitIndex = $index % $numSuits;
    return new Card(self::$orderedValues[$valueIndex],
                    self::$orderedSuits[$suitIndex]);
  }

  public function cardToIndex($card) {
    return array_search($card->value, self::$orderedValues) *
           count(self::$orderedSuits) +
           array_search($card->suit, self::$orderedSuits); 
  }

  public function findMissingCardv2($pivot, $length) {
    $length = $length/2;
    if ($this->fiftyOneCards[$pivot]->equals($this->indexToCard($pivot))) {
      $rightHalfPivot = $pivot + intval($length/2) + 1;
      if ($rightHalfPivot === count($this->cards)) {
        return $this->indexToCard(count($this->cards)-1);
      }
      return $this->findMissingCardv2($rightHalfPivot, $length);
    } else {
      if ($pivot === 0) {
        return $this->indexToCard($pivot);
      } else {
        $prevCard = $this->indexToCard($pivot-1);
        if ($this->fiftyOneCards[$pivot-1]->equals($this->indexToCard($pivot-1))) {
          return $this->indexToCard($pivot);
        }
        $leftHalfPivot = intval($pivot/2);
        return $this->findMissingCardv2($leftHalfPivot, $length);
      }
    }
  }
}
