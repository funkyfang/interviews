<?php
require_once('deck.php');

function findMissingCard($missing, $complete) {
  $pivot = intval(count($missing)/2);
  if ($complete[$pivot]->equals($missing[$pivot])) {
    if ($pivot === 0) {
      return $complete[0];
    }
    $newMissing = array_slice($missing, $pivot+1);
    $newComplete = array_slice($complete, $pivot+1);
    return findMissingCard($newMissing, $newComplete);
  } else {
    if ($pivot-1 < 0) {
      return $complete[0];
    } else if ($complete[$pivot-1]->equals($missing[$pivot-1])) {
      return $complete[$pivot]; 
    } else {
      $newMissing = array_slice($missing, 0, $pivot);
      $newComplete = array_slice($complete, 0, $pivot);
      return findMissingCard($newMissing, $newComplete);
    }
  }
}
