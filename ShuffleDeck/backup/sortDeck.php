<?php
require_once('deck.php');

function sortDeck($deck) {
  $emptyDeck = [];
  for ($i=0; $i<13; $i++) {
    $emptyDeck[$i] = array(NULL, NULL, NULL, NULL);
  }

  foreach ($deck as $card) {
    $value = $card->value;
    $suit = $card->suit;
    switch ($value) {
      case Named::Jack:
        $value = 11;
        break;
      case Named::Queen:
        $value = 12;
        break;
      case Named::King:
        $value = 13;
        break;
      case Named::Ace:
        $value = 14;
        break;
    }

    $index = $value - 2;
    switch ($suit) {
      case Suits::Clubs:
        $emptyDeck[$index][0] = $card;
        break;
      case Suits::Diamonds:
        $emptyDeck[$index][1] = $card;
        break;
      case Suits::Hearts:
        $emptyDeck[$index][2] = $card;
        break;
      case Suits::Spades:
        $emptyDeck[$index][3] = $card;
    }
  }

  $sorted = [];
  foreach ($emptyDeck as $cards) {
    $sorted = array_merge($sorted, $cards);
  }
  return $sorted;
}
