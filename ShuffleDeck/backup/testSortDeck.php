<?php
require_once('sortDeck.php');

$shuffled = $newDeck;
shuffle($shuffled);

$sorted = sortDeck($shuffled);
//$sorted[51] = new Card(Named::Ace, Suits::Diamonds);
//$sorted[51] = new Card(2, Suits::Spades);

$isSorted = 'Pass.';
for ($i=0; $i<count($deck); $i++) {
  if (!$deck[$i]->equals($sorted[$i])) {
    $isSorted = 'FAIL!';
    break;
  }
}

echo "$isSorted\n";
