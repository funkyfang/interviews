<?php

class TimeIt {
  public static function run($msg, $func, $args) {
    $func = [$func, 'test'];

    $time_pre = microtime(true);
    $pass_fail = call_user_func_array($func, $args);
    $time_post = microtime(true);
    $exec_time = $time_post - $time_pre;
    if ($exec_time < 0.001) {
      $exec_time = "";
    } else {
      $exec_time = round($exec_time, 2);
    }
    echo "$msg\t".$pass_fail."\t$exec_time\n\n";
  }
}
