<?php
require_once('Tests/ITest.php');
require_once('Sorts.php');
require_once('TimeIt.php');

/*
  // Usage Example:
  $sorts_test = new SortsTest();
  $sorts_test->run([
    // Test all the sorts with an array of 5000 ints.
    array(
      'algs' => ['fred', 'merge', 'quick'],
      'arr_len' => 5000,
    ),
    // Compare performance of Merge and Quick sorts.
    array(
      'algs' => ['merge', 'quick'],
      'arr_len' => 20000,
    ),
  ]);
*/
class SortsTest implements ITest {
  use Test;

  public function __construct() {
  }

  /*
    Expects $arr to be in the format:
    array(
      array(
        'algs' => ['sort_func1_name', 'sort_func2_name', ...],
        'arr_len' => Int,
      ),
      ...
    )
  */
  public function run($arr) {
    foreach ($arr as $val) {
      $algs = $val['algs'];
      $arr_len = $val['arr_len'];
      $random_ints = self::get_random_ints($arr_len);
      foreach ($algs as $alg) {
        TimeIt::run(
          ucwords($alg).' sort on '.$arr_len. ' ints.',
          $this,
          array([
            'alg' => $alg,
            'random_ints' => $random_ints,
          ]));
      }
    }
  }

  /*
    Expects $arr to be in the format:
    array(
      'alg' => 'sort_func_name',
      'random_ints' => [Int, Int, ...],
    )
  */
  public function test($arr) {
    $alg = $arr['alg'];
    $random_ints = $arr['random_ints'];

    $valid_sort = $random_ints;
    sort($valid_sort);

    $sorted = forward_static_call("Sorts::$alg", $random_ints);

    // Easier to compare strings of sorted ints.
    if (implode('', $sorted) == implode('', $valid_sort)) {
      return self::$pass;
    }
    return self::$fail;
  }

  private static function get_random_ints($arr_len) {
    $random_ints = [];
    // Randomize whether the list is odd or even.
    $arr_len += rand(0, 1);
    for ($i=0; $i<$arr_len; $i++) {
      // Random int range is [0, length of array].
      $random_ints[] = rand(0, $arr_len);
    }
    return $random_ints;
  }
}
