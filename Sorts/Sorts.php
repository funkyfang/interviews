<?php
require_once('DataStructures/BinaryHeap.php');

class Sorts {
  /*
    Fred Sort (aka Insertion?)
    - Compare each element in an unsorted list to the sorted list.
    - Insert the element from the unsorted list into the sorted list.
  */
  public static function fred($arr) {
    $sorted = array($arr[0]);
    for ($i=1; $i<count($arr); $i++) {
      $inserted = false;
      for ($j=0; $j<count($sorted); $j++) {
        if ($arr[$i] <= $sorted[$j]) {
          $left = array_slice($sorted, 0, $j);
          $right = array_slice($sorted, $j);
          $sorted = array_merge($left, array($arr[$i]), $right);
          $inserted = true;
          break;
        }
      }
      if (!$inserted) {
        $sorted[] = $arr[$i];
      }
    }
    return $sorted;
  }

  /*
    Merge Sort
    - Recursively split the array into:
      - Halves: right and left.
      - Return if the array has one or fewer elements.
    - Recursively build the sorted list by:
      - Comparing the first elements in the right and left lists
        and popping the appropriate element.
      - When either the right or left list are empty, append the
        remaining elements from either lists to the sorted list.
  */
  public static function merge($arr) {
    $arr_len = count($arr);
    if (count($arr) <= 1) {
      return $arr;
    } else {
      $left_len = floor($arr_len/2);
      $left = self::merge(array_slice($arr, 0, $left_len));
      $right = self::merge(array_slice($arr, $left_len));
      $sorted_arr = [];
      while (count($left)>0 && count($right)>0) {
        if ($left[0] <= $right[0]) {
          $sorted_arr[] = array_shift($left);
        } else {
          $sorted_arr[] = array_shift($right);
        }
      }
      $remaining_arr = max($left, $right);
      return array_merge($sorted_arr, $remaining_arr);
    }
  }

  /*
    Quick Sort
    - Calculate a pivot.
    - For each element in unsorted list, if:
      - Element is < pivot, append to the left list.
      - Otherwise, append to the right list.
    - Recursively continue this process until either:
      - The array has one or fewer elements or
      - The array contains identical elements.
    - Recursively merge the left and right lists.
  */
  public static function quick($arr) {
    if (count($arr) <= 1) {
      return $arr;
    } else if (count(array_unique($arr)) == 1) {
      return $arr;
    }

    $pivot = floor(array_sum($arr)/count($arr));

    $left = [];
    $right = [];
    foreach ($arr as $a) {
      if ($a<=$pivot) {
        $left[] = $a;
      } else {
        $right[] = $a;
      }
    }
    return array_merge(self::quick($left), self::quick($right));
  }

  /*
    Heap Sort
    - Insert data into a binary heap.
    - Extract the root until empty.
    - Append each root to a list.
    - Return the list.
  */
  public function heap($arr) {
    $binaryHeap = new BinaryHeap($arr);
    $sorted = [];
    $val = $binaryHeap->extract();
    while ($val !== null) {
      $sorted[] = $val;
      $val = $binaryHeap->extract();
    }
    return $sorted;
  }

  /*
    Bubble Sort
    - For all elements,
      - If e_n >= e_n+1, swap.
    - Repeat until no swaps occur.
  */
  public function bubble($arr) {
    $swapped = true;
    $i = 0;
    while ($swapped) {
      $swapped = false;
      for ($i=0; $i<count($arr)-1; $i++) {
        if ($arr[$i] > $arr[$i+1]) {
          $temp = $arr[$i];
          $arr[$i] = $arr[$i+1];
          $arr[$i+1] = $temp;
          $swapped = true;
        }
      }
    }
    return $arr;
  }
}
