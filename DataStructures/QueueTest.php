<?php
require_once('Tests/ITest.php');
require_once('Queue.php');
require_once('TimeIt.php');

/*
  // Usage Example:
  $queue_test = new QueueTest();
  $queue_test->run([
    // Enqueue a.
    array(
      'func' => 'enqueue',
      'funcArgs' => ['a'],
      'validResult' => array(
        'first' => 'a',
        'last' => 'a',
      ),
    ),
    // Enqueue b.
    array(
      'func' => 'enqueue',
      'funcArgs' => ['b'],
      'validResult' => array(
        'first' => 'a',
        'last' => 'b',
      ),
    ),
    // Dequeue a.
    array(
      'func' => 'dequeue',
      'funcArgs' => [],
      'validResult' => array(
        'first' => 'b',
        'last' => 'b',
      ),
    ),
  ]);
*/

class QueueTest implements ITest {
  use Test;

  private $queueLinkedList;
  private $queueStack;
  private $queueArr;

  public function __construct() {
    $this->queueLinkedList = new QueueLinkedList();
    $this->queueStack = new QueueStack();
    $this->queueArr = new QueueArr();
  }

  /*
    Expects $arr to be in the format:
    array(
      'func' => "Stack function name (e.g., 'push')",
      'funcArgs' => [a list of args passed into the func],
      'validResult' => array(
        'first' => "The value of the first element",
        'last' => "The value of the last element",
      ),
    ),
    ...
  */
  public function run($arr) {
    foreach ($arr as $test) {
      $func = $test['func'];
      $funcArgs = $test['funcArgs'];
      $validResult = $test['validResult'];

      $prevFirst = $this->queueLinkedList->peakFirst();
      $prevLast = $this->queueLinkedList->peakLast();
      call_user_func_array(array($this->queueLinkedList, $func), $funcArgs);
      call_user_func_array(array($this->queueStack, $func), $funcArgs);
      call_user_func_array(array($this->queueArr, $func), $funcArgs);
      $currFirst = $this->queueLinkedList->peakFirst();
      $currLast = $this->queueLinkedList->peakLast();

      TimeIt::run(
        "Queue $func ".implode(', ', $funcArgs).".\n".
          "Previous first, last: $prevFirst, $prevLast; ".
          "Current first, last: $currFirst, $currLast",
        $this,
        [$validResult]);
    }
  }

  public function test($validResult) {
    $validFirst = $validResult['first'];
    $validLast = $validResult['last'];

    if ($this->queueLinkedList->peakFirst() !== $this->queueStack->peakFirst() ||
        $this->queueLinkedList->peakLast() !== $this->queueStack->peakLast()) {
      return self::$fail;
    }

    if ($this->queueLinkedList->peakFirst() !== $this->queueArr->peakFirst() ||
        $this->queueLinkedList->peakLast() !== $this->queueArr->peakLast()) {
      return self::$fail;
    }

    if ($this->queueLinkedList->peakFirst() !== $validFirst ||
        $this->queueLinkedList->peakLast() !== $validLast) {
      return self::$fail;
    }

    return self::$pass;
  }
}
