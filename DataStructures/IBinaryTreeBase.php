<?php

interface IBinaryTreeBase {
  public function __construct($arr);
  public function insert($arr);
  public function search($val, $node=null);
  public function delete($val, $node=null);
}

trait BinaryTreeBase {
  private $root;

  /*
    Insert
    - If val < node's data,
      - If node has left node, recurse on left node.
      - Otherwise insert val into node's left node.
    - Otherwise,
      - If node has right node, recurse on right node.
      - Otherwise insert val into node's right node.
  */
  private function _insert($node, $val) {
    $left_node = $node->left_node;
    $right_node = $node->right_node;

    if ($val < $node->data) {
      if ($left_node) {
        $this->_insert($left_node, $val);
      } else {
        $reflect = new ReflectionClass(get_class($node));
        $instance = $reflect->newInstance($val, $node->data);
        $node->left_node = $instance;
      }
    } else {
      if ($right_node) {
        $this->_insert($right_node, $val);
      } else {
        $reflect = new ReflectionClass(get_class($node));
        $instance = $reflect->newInstance($val, $node->data);
        $node->right_node = $instance;
      }
    }
  }

  /*
    Search
    - If parent's data = val, return node.
    - If val < parent's data, recurse on left node.
    - Otherwise, recurse on right node.
    - Otherwise, return null.
  */
  public function search($val, $node=null) {
    if (!$node) {
      $node = $this->root;
    }

    if ($node->data == $val) {
      return $node;
    } else if ($val<$node->data && $node->left_node) {
      return $this->search($val, $node->left_node);
    } else if ($node->right_node) {
      return $this->search($val, $node->right_node);
    }

    return null;
  }

  /*
    Delete
    - Find the node.
    - Find the node's left maximum value.
    - Replace the node with a new node whose value is the left maximum value.
    - Delete the node with the maximum value. Recurse as necessary.
  */
  public function _delete($val, &$node=null, &$new_val=null) {
    if (!$node) {
      $node = &$this->root;
    }

    if ($node->data == $val) {
      $max_val = self::maxVal($node->left_node);
      if ($max_val) {
        $new_val = $max_val;
        $reflect = new ReflectionClass(get_class($node));
        $instance = $reflect->newInstance($max_val, $node->parent_data);
        $new_node = $instance;
        $new_node->left_node = $node->left_node;
        if ($new_node->left_node) {
          $new_node->left_node->parent_data = $new_node->data;
        }
        $new_node->right_node = $node->right_node;
        if ($new_node->right_node) {
          $new_node->right_node->parent_data = $new_node->data;
        }
        $node = $new_node;
        if ($node->left_node) {
          $this->_delete($max_val, $node->left_node, $new_val);
        }
      } else if ($node->right_node) {
        $parent_data = $node->parent_data;
        $node = $node->right_node;
        $node->parent_data = $parent_data;
        $new_val = $node->data;
      } else {
        $node = null;
      }
    } else if ($val<$node->data && $node->left_node) {
      $this->_delete($val, $node->left_node, $new_val);
    } else if ($node->right_node) {
      $this->_delete($val, $node->right_node, $new_val);
    }
    return $new_val;
  }

  /*
    Find the maximum value in a tree.
    - Since left < right, if right exists, recurse.
    - Otherwise, return parent's data.
  */
  public static function maxVal($node) {
    if ($node->right_node) {
      return self::maxVal($node->right_node);
    }
    return $node->data;
  }

  /*
    PreOrder traversal
    - Parent.
    - Left.
    - Right.
  */
  public function preOrderList($node=null) {
    if (!$node) {
      $node = $this->root;
    }

    $list = [$node->data];
    if ($node->left_node) {
      $list = array_merge($list, $this->preOrderList($node->left_node));
    }
    if ($node->right_node) {
      $list = array_merge($list, $this->preOrderList($node->right_node));
    }
    return $list;
  }

  /*
    InOrder traversal
    - Left.
    - Parent.
    - Right.
  */
  public function inOrderList($node=null) {
    if (!$node) {
      $node = $this->root;
    }

    $list = [];
    if ($node->left_node) {
      $list = array_merge($list, $this->inOrderList($node->left_node));
    }
    $list[] = $node->data;
    if ($node->right_node) {
      $list = array_merge($list, $this->inOrderList($node->right_node));
    }
    return $list;
  }

  /*
    PostOrder traversal
    - Left.
    - Right.
    - Parent.
  */
  public function postOrderList($node=null) {
    if (!$node) {
      $node = $this->root;
    }

    $list = [];
    if ($node->left_node) {
      $list = array_merge($list, $this->postOrderList($node->left_node));
    }
    if ($node->right_node) {
      $list = array_merge($list, $this->postOrderList($node->right_node));
    }
    $list[] = $node->data;
    return $list;
  }

  /*
    LevelOrder traversal
    - Top to bottom.
    - Left to right.
    - Non-recursive using an array.
    - Iterate until array has no elements.
  */
  public function levelOrderlist($node=null, $level=0) {
    $nodes = [];
    if (!$node) {
      $nodes[] = $this->root;
    } else {
      $nodes[] = $node;
    }
    $list = [];
    while (count($nodes) > 0) {
      $list[] = $this->_nextLevel($nodes);
    }
    return $list;
  }

  /*
    LevelOrder next
    - Remove first element of array.
    - Append left node to array.
    - Append right node to array.
    - Return first element's data.
  */
  private function _nextLevel(&$nodes) {
    $node = array_shift($nodes);
    if ($node->left_node) {
      $nodes[] = $node->left_node;
    }
    if ($node->right_node) {
      $nodes[] = $node->right_node;
    }
    return $node->data;
  }
}

class BinaryTreeNode {
  public $data;
  public $left_node;
  public $right_node;

  public function __construct($data) {
    $this->data = $data;
  }
}
