<?php
require_once('Tests/ITest.php');
require_once('LinkedList.php');
require_once('TimeIt.php');

/*
  // Usage Example:
  $linkedlist_test = new LinkedListTest();
  $linkedlist_test->run([
    // Append d to list.
    array(
      'func' => 'append',
      'funcArgs' => ['d'],
      'validStr' => 'd',
    ),
    // Append e to list
    array(
      'func' => 'append',
      'funcArgs' => ['e'],
      'validStr' => 'de',
    ),
  ]);
*/

class LinkedListTest implements ITest {
  use Test;

  private $linkedList;

  public function __construct() {
    $this->linkedList = new LinkedList();
  }

  /*
    Expects $arr to be in the format:
    array(
      'func' => "LinkedList function name (e.g., 'append')",
      'funcArgs' => [a list of args passed into the func],
      'validStr' => "A string representation of the linked list (e.g., A->B is 'AB')",
    ),
    ...
  */
  public function run($arr) {
    foreach ($arr as $test) {
      $func = $test['func'];
      $funcArgs = $test['funcArgs'];
      $validStr = $test['validStr'];

      $beforeStr = $this->linkedList->toString();
      call_user_func_array(array($this->linkedList, $func), $funcArgs);
      $afterStr = $this->linkedList->toString();

      TimeIt::run(
        "Linked list $func ".implode(', ', $funcArgs).".\n".
          "Before: $beforeStr, After: $afterStr",
        $this,
        [$validStr]);
    }
  }

  public function test($validStr) {
    if ($this->linkedList->toString() === $validStr) {
      return self::$pass;
    }
    return self::$fail;
  }
}
