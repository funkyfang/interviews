<?php
require_once('LinkedList.php');

/*
  Stack
  - First in, last out.
  - Keep track of the top.
  - Push to the top.
  - Pop from the top.
*/

class Stack {
  protected $linkedList;

  public function __construct($arr=[]) {
    $this->linkedList = new LinkedList();
    foreach ($arr as $data) {
      $this->push($data);
    }
  }

  /*
    The top of the stack is the head of the linked list.
    So pushing, prepends the data.
    Prepend is O(1).
  */
  public function push($data) {
    $this->linkedList->prepend($data);
  }

  /*
    The top of the stack is the head of the linked list.
    So popping, deletes the head and returns it's value.
    Deleting the head is O(1).
  */
  public function pop() {
    if (!$this->isEmpty()) {
      $headData = $this->linkedList->head->data;
      $this->linkedList->delete($headData);
      return $headData;
    }
    return null;
  }

  /*
    Peak the top's data without popping.
  */
  public function peak() {
    return $this->linkedList->head->data;
  }

  public function isEmpty() {
    return $this->linkedList->head === null;
  }
}

/*
  Stack (implemented as array)
  - Push appends to the array.
  - Pop removes the last element of the array.
  - Keep track of the top index.
*/
class StackArr {
  private $arr;
  private $topIndex;
  private static $emptyIndex = -1;

  public function __construct($arr=[]) {
    $this->arr = [];
    $this->topIndex = self::$emptyIndex;
    foreach ($arr as $data) {
      $this->push($data);
    }
  }

  public function push($data) {
    $this->topIndex++;
    $this->arr[$this->topIndex] = $data;
  }

  public function pop() {
    if ($this->isEmpty()) {
      return null;
    }
    $data = $this->arr[$this->topIndex];
    $this->topIndex--;
    return $data;
  }

  public function peak() {
    if ($this->isEmpty()) {
      return null;
    }
    return $this->arr[$this->topIndex];
  }

  public function isEmpty() {
    return $this->topIndex === self::$emptyIndex;
  }
}

/*
  Min Stack
  - Same as stack.
  - But has a method to peak at the min value in the stack.
*/
class MinStack extends Stack {
  private $min;

  public function push($data) {
    parent::push($data);
    if (!$this->min) {
      $this->min = $data;
    } else if ($data < $this->min) {
      $this->min = $data;
    }
  }

  public function pop() {
    if ($data = parent::pop()) {
      if ($data === $this->min) {
        $this->setMin();
        return $data;
      }
    }
    return null;
  }

  public function peakMin() {
    return $this->min;
  }

  private function setMin() {
    $node = $this->linkedList->head;
    $this->min = $node->data;
    while ($node = $node->next) {
      if ($node->data < $this->min) {
        $this->min = $node->data;
      }
    }
  }
}
