<?php
require_once('Tests/ITest.php');
require_once('Stack.php');
require_once('TimeIt.php');

/*
  // Usage Example:
  $stack_test = new StackTest();
  $stack_test->run([
    // Push a to stack.
    array(
      'func' => 'push',
      'funcArgs' => ['a'],
      'validTop' => 'a',
      'validMin' => 'a',
    ),
    // Push b to stack.
    array(
      'func' => 'push',
      'funcArgs' => ['b'],
      'validTop' => 'b',
      'validMin' => 'a',
    ),
    // Pop b from stack.
    array(
      'func' => 'pop',
      'funcArgs' => [],
      'validTop' => 'a',
      'validMin' => 'a',
    ),
  ]);
*/

class StackTest implements ITest {
  use Test;

  private $stack;
  private $stackArr;
  private $minStack;

  public function __construct() {
    $this->stack = new Stack();
    $this->stackArr = new StackArr();
    $this->minStack = new MinStack();
  }

  /*
    Expects $arr to be in the format:
    array(
      'func' => "Stack function name (e.g., 'push')",
      'funcArgs' => [a list of args passed into the func],
      'validTop' => "The value of the top element",
    ),
    ...
  */
  public function run($arr) {
    foreach ($arr as $test) {
      $func = $test['func'];
      $funcArgs = $test['funcArgs'];
      $validTop = $test['validTop'];
      $validMin = $test['validMin'];

      $prevTop = $this->stack->peak();
      call_user_func_array(array($this->stack, $func), $funcArgs);
      call_user_func_array(array($this->stackArr, $func), $funcArgs);
      call_user_func_array(array($this->minStack, $func), $funcArgs);
      $currTop = $this->stack->peak();

      TimeIt::run(
        "Stack $func ".implode(', ', $funcArgs).".\n".
          "Previous top: $prevTop, Current top: $currTop",
        $this,
        [[$validTop, $validMin]]);
    }
  }

  public function test($arr) {
    $validTop = $arr[0];
    $validMin = $arr[1];

    if ($this->stack->peak() !== $this->stackArr->peak()) {
      return self::$fail;
    }

    if ($this->stack->peak() !== $this->minStack->peak()) {
      return self::$fail;
    }

    if ($this->minStack->peakMin() !== $validMin) {
      return self::$fail;
    }

    if ($this->stack->peak() !== $validTop) {
      return self::$fail;
    }

    return self::$pass;
  }
}
