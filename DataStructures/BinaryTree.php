<?php
require_once('IBinaryTreeBase.php');

class BinaryTree implements IBinaryTreeBase {
  use BinaryTreeBase;

  public function __construct($arr) {
    $this->root = new BinaryTreeNode(array_shift($arr));
    $this->insert($arr);
  }

  public function insert($arr) {
    while (count($arr) > 0) {
      $this->_insert($this->root, array_shift($arr));
    }
  }

  public function delete($val, $node=null) {
    $this->_delete($val, $node);
  }
}
