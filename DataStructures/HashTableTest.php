<?php
require_once('Tests/ITest.php');
require_once('HashTable.php');
require_once('TimeIt.php');

/*
  // Usage Example:
  // HashTable testing with 15000 keys.
  $hashtable_test = new HashTableTest(15000);
  // Compare performance bewteen HashTable with 10000 buckets and 1 bucket.
  $hashtable_test->run([
    10000,
    1,
  ]);
*/
class HashTableTest implements ITest {
  use Test;

  private $keys;
  private $assoc_arr = [];

  public function __construct($num_keys) {
    // Generate random keys.
    $this->keys = self::random_strings($num_keys);

    // Contruct assoc array for validation purposes.
    foreach ($this->keys as $i => $key) {
      $this->assoc_arr[$key] = $i;
    }
  }

  public function run($ht_sizes) {
    foreach ($ht_sizes as $size) {
      $ht = new HashTable($size);
      foreach ($this->keys as $i => $key) {
        $ht->set($key, $i);
      }
      TimeIt::run(
        "HashTable size of $size",
        $this,
        [$ht]);
    }
  }

  public function test($ht) {
    // Make copies so that one instance can re-run.
    $keys = $this->keys;
    $assoc_arr = $this->assoc_arr;

    // Test update.
    shuffle($keys);
    $i = $keys[0];
    $new_value = 'new_value';
    $assoc_arr[$i] = $new_value;
    $ht->set($i, $new_value);

    // Compare arrays.
    $diff = array_diff($assoc_arr, $ht->get_assoc_arr());
    if (empty($diff)) {
      return self::$pass;
    }
    return self::$fail;
  }

  /*
    Generate random strings between 1 and 10 chars.
    Chars are a-zA-Z.
  */
  private static function random_strings($num) {
    $chars = 'abcdefghijklmnopqrstuvwxyz';
    $chars .= strtoupper(chars);
    $chars = str_split($chars);

    $strings = [];
    for ($i=0; $i<$num; $i++) {
      $len = rand(1, 10);
      $random_string = '';
      for ($j=0; $j<$len; $j++) {
        shuffle($chars);
        $random_string .= $chars[0];
      }
      $strings[] = $random_string;
    }
    return $strings;
  }
}
