<?php

/*
Singly Linked List
- A linked list has a head property which points to the first node.
- A node has a data property and a next property. The next property
  points to the next node.
*/

class LinkedList {
  public $head;

  public function __construct() {
    $this->head = null;
  }

  /*
    Prepend
    - Create a new node and set its next to the head.
    - Set the head to the new node.
  */
  public function prepend($data) {
    $this->head = new Node($data, $this->head);
  }

  /*
    Append
    - If the list is empty,
      - Create a new node.
      - Set the head to the new node.
    - Else find the last node (i.e., next is null),
      - Create a new node.
      - Set the last node's next to the new node.
  */
  public function append($data) {
    if ($this->head === null) {
      $this->head = new Node($data);
    } else {
      $node = $this->head;
      while ($node->next !== null) {
        $node = $node->next;
      }
      $node->next = new Node($data);
    }
  }

  /*
    Insert after
    - Set the current node to the head.
    - While the current node is not null,
      - If the key matches the current node's data,
        - Create a new node and set its next to the current node's next.
        - Set the current node's next to the new node.
        - Break.
    - Else iterate by setting the current node to the current node's next.
  */
  public function insertAfter($key, $data) {
    $node = $this->head;
    while ($node !== null) {
      if ($node->data === $key) {
        $node->next = new Node($data, $node->next);
        break;
      } else {
        $node = $node->next;
      }
    }
  }

  /*
    Delete
    - Set the previous node to null.
    - Set the current node to the head.
    - While the current node is not null,
      - If the key matches,
        - If the current node is the head, set the head to the current node's next.
        - Else, set the previous node's next to the current node's next.
    - Else iterate by
      - Setting the previous node to the current node.
      - Setting the current node to the current node's next.
  */
  public function delete($key) {
    $prev = null;
    $node = $this->head;
    while ($node !== null) {
      if ($node->data === $key) {
        if ($node->data === $this->head->data) {
          $this->head = $node->next;
        } else {
          $prev->next = $node->next;
        }
        break;
      } else {
        $prev = $node;
        $node = $node->next;
      }
    }
  }

  /*
    For testing.
  */
  public function toString() {
    $str = "";
    $node = $this->head;
    while ($node !== null) {
      $str .= $node->data;
      $node = $node->next;
    }
    return $str;
  }
}

class Node {
  public $data;
  public $next;

  public function __construct($data, $next = null) {
    $this->data = $data;
    $this->next = $next;
  }
}
