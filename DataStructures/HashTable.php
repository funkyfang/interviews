<?php

/*
Hash Table
- A hash function maps an arbitrary number of keys into a fixed number of buckets.
- A good hash function evenly distributes keys into buckets.
- To calculate the index of a bucket, hash value % number of buckets.
- For conflict resolution, each bucket is an list of entries.
- Each entry stores the key, value.
- Set function.
  - Calculate the bucket index and access bucket.
  - If bucket empty, append.
  - Otherwise, search the bucket's list for the key.
    - If key found, overwrite the entry's value.
    - Otherwise, append a new entry.
- Get function.
  - Calculate the bucket index and access bucket.
  - If the bucket has a list, search entries for the key.
    - If key found, return value.
  - Otherwise, return null.
*/

class HashTable {
  private $buckets = [];

  /*
    Construct table with predefined number of buckets.
  */
  public function __construct($num_buckets) {
    for ($i=0; $i<$num_buckets; $i++) {
      $this->buckets[] = [];
    }
  }

  /*
    Get bucket by key.
    Scan entries in bucket for key.
      If key exists, overwrite value.
    Otherwise, add new entry to bucket.
  */
  public function set($key, $val) {
    $key_exists = false;
    // Reference to bucket.
    $bucket = &$this->buckets[$this->get_bucket_index($key)];
    foreach ($bucket as $entry) {
      if ($key == $entry->key) {
        $entry->val = $val;
        $key_exists = true;
        break;
      }
    }
    if (!$key_exists) {
      $bucket[] = new Entry($key, $val);
    }
  }

  /*
    Get bucket by key.
    Scan entries in bucket for key.
      If key exists, return value.
    Otherwise, return null.
  */
  public function get($key) {
    $bucket = $this->buckets[$this->get_bucket_index($key)];
    foreach ($bucket as $entry) {
      if ($key == $entry->key) {
        return $entry->val;
      }
    }
    return null;
  }

  /*
    For testing.
  */
  public function get_assoc_arr() {
    $assoc_arr = [];
    foreach ($this->buckets as $bucket) {
      foreach ($bucket as $entry) {
        $assoc_arr[$entry->key] = $entry->val;
      }
    }
    return $assoc_arr;
  }

  /*
    Calculate bucket index by: hash value % number of buckets.
  */
  private function get_bucket_index($key) {
    return self::hash($key) % count($this->buckets);
  }

  /*
    Convert chars in a string to an int.
    Return sum of ints.
  */
  private static function hash($key) {
    $hash = 0;
    foreach (str_split($key) as $char) {
      $hash += ord($char);
    }
    return $hash;
  }
}

/*
  For conflict resolution, each bucket contains a list of entries.
*/
class Entry {
  public $key;
  public $val;

  public function __construct($key, $val) {
    $this->key = $key;
    $this->val = $val;
  }
}
