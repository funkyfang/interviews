<?php
require_once('LinkedList.php');
require_once('Stack.php');

/*
  QueueLinkedList
  - First in, first out.
  - Head is the the first node.
  - Create pointer to the last node.
  - Enqueue by
    - Create a new node.
    - Set the current last node's next to the new node.
    - Set the current last node to the new node.
  - Dequeue by
    - Delete the head.
    - Return the head's value. 
*/
class QueueLinkedList {
  private $linkedList;
  private $lastNode;

  public function __construct($arr=[]) {
    $this->linkedList = new LinkedList();
    foreach ($arr as $data) {
      $this->enqueue($data);
    }
  }

  public function dequeue() {
    if ($head = $this->linkedList->head) {
      $headData = $head->data;
      $this->linkedList->delete($headData);
      return $headData;
    }
    return null;
  }

  public function enqueue($data) {
    $newNode = new Node($data);
    if ($this->lastNode) {
      $this->lastNode->next = $newNode;
    } else {
      $this->linkedList->head = $newNode;
    }
    $this->lastNode = $newNode;
  }

  /*
    Peak the first node's data without dequeueing.
  */
  public function peakFirst() {
    return $this->linkedList->head->data;
  }

  public function peakLast() {
    return $this->lastNode->data;
  }
}

/*
  QueueStack
  - First in, first out.
  - In stack's top is the last node.
  - Out stack's top is the first node.
  - Enqueue by pushing into the in stack.
  - Dequeue by
    - If no top, pop in stack into out stack (reverse).
    - Pop top and return.
*/
class QueueStack {
  private $inStack;
  private $outStack;

  public function __construct($arr=[]) {
    $this->inStack = new Stack();
    $this->outStack = new Stack();
    foreach ($arr as $data) {
      $this->enqueue($data);
    }
  }

  public function dequeue() {
    $this->popInStack();
    return $this->outStack->pop();
  }

  public function enqueue($data) {
    $this->inStack->push($data);
  }

  /*
    Peaking the first node's data only works if the outStack is not empty.
  */
  public function peakFirst() {
    $this->popInStack();
    return $this->outStack->peak();
  }

  /*
    Peaking the last node's data onl works if the inStack is not empty.
  */
  public function peakLast() {
    $this->popOutStack();
    return $this->inStack->peak();
  }

  /*
    Pop the in stack into the out stack (reverse).
    The out stack's top is the first node.
  */
  private function popInStack() {
    if ($this->outStack->isEmpty()) {
      while (!$this->inStack->isEmpty()) {
        $this->outStack->push($this->inStack->pop());
      }
    }
  }

  /*
    Pop the out stack into the in stack (reverse).
    The in stack's top is the last node.
  */
  private function popOutStack() {
    if ($this->inStack->isEmpty()) {
      while (!$this->outStack->isEmpty()) {
        $this->inStack->push($this->outStack->pop());
      }
    }
  }
}

/*
  Queue (implemented as array)
  - Track first and last index.
  - Enqueue:
    - Increment last index.
    - Set value for last index.
  - Dequeue:
    - If first index <= last index,
      - Return data for first index.
      - Increment first index.
    - Else return null.
*/
class QueueArr {
  private $arr;
  private $firstIndex;
  private $lastIndex;

  public function __construct($arr=[]) {
    $this->arr = [];
    foreach ($arr as $data) {
      $this->enqueue($data);
    }
  }

  public function enqueue($data) {
    if ($this->lastIndex !== null) {
      $this->lastIndex++;
    } else {
      $this->firstIndex = 0;
      $this->lastIndex = 0;
    }
    $this->arr[$this->lastIndex] = $data;
  }

  public function dequeue() {
    if ($this->firstIndex <= $this->lastIndex) {
      $data = $this->arr[$this->firstIndex];
      $this->firstIndex++;
      return $data;
    }
    return null;
  }

  public function peakFirst() {
    if ($this->firstIndex <= $this->lastIndex) {
      return $this->arr[$this->firstIndex];
    }
    return null;
  }

  public function peakLast() {
    if ($this->firstIndex <= $this->lastIndex) {
      return $this->arr[$this->lastIndex];
    }
    return null;
  }
}
