<?php
require_once('IBinaryTreeBase.php');

class RedBlackTree implements IBinaryTreeBase {
  use BinaryTreeBase;

  public function __construct($arr) {
    $this->root = new RedBlackTreeNode(array_shift($arr));
    $this->_insert_case1($this->root);
    $this->insert($arr);
  }

  public function insert($arr) {
    while (count($arr) > 0) {
      $val = array_shift($arr);
      $this->_insert($this->root, $val);
//echo "Start:\n";
//print_r($this->root);
      $node = $this->search($val);
      $this->_insert_case1($node);
//echo "root:\n";
//print_r($this->root);
    }
  }

  private function parent($node) {
    return $this->search($node->parent_data);
  }

  private function grandparent($node) {
    $p = $this->parent($node);
    return $this->parent($p);
  }

  private function uncle($node) {
    $g = $this->grandparent($node);
    if ($g) {
      if ($node->parent_data == $g->left_node->data) {
        return $g->right_node;
      }
      return $g->left_node;
    }
    return null;
  }

  private function sibling($node) {
    $p = $this->parent($node);
    if ($node && $p) {
      if ($node->data == $p->left_node->data) {
        return $p->right_node;
      }
      return $p->left_node;
    }
    return null;
  }

  private function _insert_case1($node) {
    if ($node->parent_data == null) {
      $this->root->color = RedBlackTreeNode::$black;
    } else {
      $this->_insert_case2($node);
    }
  }

  private function _insert_case2($node) {
    $p = $this->parent($node);
    if ($p->color == RedBlackTreeNode::$black) {
      return;
    } else {
      $this->_insert_case3($node);
    }
  }

  private function _insert_case3($node) {
    $u = $this->uncle($node);

    if ($u && $u->color==RedBlackTreeNode::$red) {
      $g = $this->grandparent($node);
      $g->color = RedBlackTreeNode::$red;
      $g->left_node->color = RedBlackTreeNode::$black;
      $g->right_node->color = RedBlackTreeNode::$black;
      $this->_insert_case1($g);
    } else {
      $this->_insert_case4($node);
    }
  }

  private function _insert_case4($node) {
    $g = $this->grandparent($node);
    $p = $this->parent($node);
    if ($node->data == $p->right_node->data &&
        $p->data == $g->left_node->data) {
      $rotated = $this->rotateLeft($p);
      $node = $rotated->left_node;
    } else if ($node->data == $p->left_node->data &&
               $p->data == $g->right_node->data) {
      $rotated = $this->rotateRight($p);
      $node = $rotated->right_node;
    }
    $this->_insert_case5($node);
  }

  private function _insert_case5($node) {
    $g = $this->grandparent($node);
    $g->color = RedBlackTreeNode::$red;
    if ($g->left_node->data == $node->parent_data) {
      $p = $g->left_node;
      $g->left_node->color = RedBlackTreeNode::$black;
    } else {
      $p = $g->right_node;
      $g->right_node->color = RedBlackTreeNode::$black;
    }

    if ($node->data == $p->left_node->data) {
      $this->rotateRight($g);
    } else {
      $this->rotateLeft($g);
    }
  }

  private function rotateLeft($parent) {
    $new_parent = $parent->right_node;
    $new_parent->parent_data = $parent->parent_data;
    $new_right_node = $new_parent->left_node;
    $new_parent->left_node = $parent;
    $new_parent->left_node->parent_data = $new_parent->data;
    $new_parent->left_node->right_node = $new_right_node;
    $this->replace($new_parent);
    return $new_parent;
  }

  private function rotateRight($parent) {
    $new_parent = $parent->left_node;
    $new_parent->parent_data = $parent->parent_data;
    $new_left_node = $new_parent->right_node;
    $new_parent->right_node = $parent;
    $new_parent->right_node->parent_data = $new_parent->data;
    $new_parent->right_node->left_node = $new_left_node;
    $this->replace($new_parent);
    return $new_parent;
  }

  private function replace($replacement, $node=null) {
    if (!$node) {
      $node = $this->root;
    }

    if ($replacement->parent_data == null) {
      $this->root = $replacement;
      return;
    } else if ($node->data == $replacement->parent_data) {
      if ($node->data < $replacement->data) {
        $node->right_node = $replacement;
        return;
      }
      $node->left_node = $replacement;
      return;
    }

    if ($replacement->parent_data<$node->data && $node->left_node) {
      $this->replace($replacement, $node->left_node);
    } else if ($node->right_node) {
      $this->replace($replacement, $node->right_node);
    }
  }

  public function delete($val, $node=null) {
//echo "Delete $val\n";
    $deleted_node = $this->search($val);
    $p = $this->parent($deleted_node);
    $new_val = $this->_delete($val, $node);
    $new_node = $this->search($new_val);
    if ($deleted_node->color == RedBlackTreeNode::$red ||
        $new_node->color == RedBlackTreeNode::$red) {
      if ($new_node) {
        $new_node->color = RedBlackTreeNode::$black;
      }
    } else {
      $this->_foo($new_node, $p);
    }
    if ($this->root->color == RedBlackTreeNode::$red) {
      $this->root->color = RedBlackTreeNode::$black;
    }
  }

  private function _foo($node, $p) {
    if ($node->data) {
      $s = $this->sibling($node->data);
    } else {
      $s = ($p->left_node) ? $p->left_node : $p->right_node;
    }
//echo "Sibling:\n";
//print_r($s);
    if ($s->color == RedBlackTreeNode::$black) {
      $both_red = $s->right_node->color == RedBlackTreeNode::$red &&
        $s->left_node->color == RedBlackTreeNode::$red;
      $r_is_right_child = $s->right_node->color == RedBlackTreeNode::$red;
      $r_is_left_child = $s->left_node->color == RedBlackTreeNode::$red;

      if ($s->data == $p->left_node->data &&
          ($r_is_left_child || $both_red)) {
        $new_p = $this->rotateRight($p);
        $new_p->left_node->color = RedBlackTreeNode::$black;
      } else if ($s->data == $p->left_node->data && $r_is_right_child) {
        $this->rotateLeft($s);
        $new_parent = $this->rotateRight($p);
//        if (!$new_parent->parent_data) {
//          $new_parent->color = RedBlackTreeNode::$black;
//        }
      } else if ($s->data == $p->right_node->data &&
                 ($r_is_right_child || $both_red)) {
        $new_p = $this->rotateLeft($p);
        $new_p->right_node->color = RedBlackTreeNode::$black;
      } else if ($s->data == $p->right_node->data && $r_is_left_child) {
        $this->rotateRight($s);
        $new_parent = $this->rotateLeft($p);
//        if (!$new_parent->parent_data) {
//          $new_parent->color = RedBlackTreeNode::$black;
//        }
      } else {
        if ((!$s->left_node || $s->left_node->color == RedBlackTreeNode::$black) &&
            (!$s->right_node || $s->right_node->color == RedBlackTreeNode::$black)) {
          $s->color = RedBlackTreeNode::$red;
//echo "Parent:\n";
          if ($p->color == RedBlackTreeNode::$black &&
              $p->parent_data) {
            $this->_foo($p, $this->parent($p));
          }
        }
      }
    } else {
      if ($s->data == $p->right_node->data) {
        $this->rotateLeft($p);
      } else {
        $this->rotateRight($p);
      }
    }
  }
}

class RedBlackTreeNode extends BinaryTreeNode {
  public static $red = 'red';
  public static $black = 'black';
  public $color;
  public $parent_data;

  public function __construct($data, $parent_data=null) {
    parent::__construct($data);
    $this->color = self::$red;
    $this->parent_data = $parent_data;
  }
}
