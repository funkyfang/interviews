<?php

/*
  Binary Heap
  - A complete binary tree (level order inserted).
  - Every node has 0, 1, or 2 children.
  - The parent node's value is less than the children nodes' value.
*/

class BinaryHeap {
  // Implementation relies on an array.
  public $arr;

  public function __construct($arr = []) {
    $this->arr = [];
    foreach ($arr as $val) {
      $this->insert($val);
    }
  }

  /*
    Get the parent's index from the child's index.
  */
  public function parentIndex($childIndex) {
    return intval($childIndex/2);
  }

  /*
    Get the left child's index from the parent's index.
  */
  public function leftChildIndex($parentIndex) {
    if ($parentIndex) {
      return 2*$parentIndex;
    }
    // If the parent's index is 0, then its left child's index is 1.
    return 1;
  }

  /*
    Get the right child's index from the left child's index.
  */
  public function rightChildIndex($parentIndex) {
    // The right child is always next to the left child.
    return $this->leftChildIndex($parentIndex)+1;
  }

  /*
    Insert
    - Append the new value.
    - Calculate the new value's parent's index.
    - While the parent's value > the new value,
      - Swap the values.
      - Assign the child's index value to the parent's index value.
      - Calculate the new parent index.
  */
  public function insert($val) {
    $this->arr[] = $val;
    $childIndex = count($this->arr)-1;
    $parentIndex = $this->parentIndex($childIndex);
    while ($this->arr[$parentIndex] > $this->arr[$childIndex]) {
      $childVal = $this->arr[$childIndex];
      $this->arr[$childIndex] = $this->arr[$parentIndex];
      $this->arr[$parentIndex] = $childVal;
      $childIndex = $parentIndex;
      $parentIndex = $this->parentIndex($childIndex);
    }
  }

  /*
    Extract
    - Shift off the first element (root) from the array.
    - Pop the last element off the array.
    - Prepend the last element.
    - Heapify down.
    - Return the root.
  */
  public function extract() {
    $root = array_shift($this->arr);
    $lastElement = array_pop($this->arr);
    array_unshift($this->arr, $lastElement);
    $this->heapifyDown(0);
    return $root;
  }

  /*
    Heapify Down
    - Calculate the parent's left child index.
    - Calculate the parent's right child index.
    - Set the current minimum value to the parent's value.
    - If the left child's index exists and its value is less than the current
      minimum value, set the new parent index to its index, and set the new
      current minimum value to its value.
    - If the right child's index exists and its value is less than the current
      minimum value, set the new parent index to its index, and set the current
      minimum value to its value.
    - If there's a new parent index,
      - Swap values with the old parent index.
      - Recurse on the new parent index.
  */
  private function heapifyDown($parentIndex) {
    $leftChildIndex = $this->leftChildIndex($parentIndex);
    $rightChildIndex = $this->rightChildIndex($parentIndex);
    $newParentIndex = null;
    $minVal = $this->arr[$parentIndex];
    if (array_key_exists($leftChildIndex, $this->arr) &&
      $this->arr[$leftChildIndex] < $minVal) {
      $newParentIndex = $leftChildIndex;
      $minVal = $this->arr[$leftChildIndex];
    }
    if (array_key_exists($rightChildIndex, $this->arr) &&
      $this->arr[$rightChildIndex] < $minVal) {
      $newParentIndex = $rightChildIndex;
      $minVal = $this->arr[$rightChildIndex];
    }
    if ($newParentIndex) {
      $this->arr[$newParentIndex] = $this->arr[$parentIndex];
      $this->arr[$parentIndex] = $minVal;
      $this->heapifyDown($newParentIndex);
    }
  }
}

class BinaryHeapTree {
  public $head;
  public $tail;

  public function __construct($arr=[]) {
    $first = array_shift($arr);
    $this->head = new HeapNode($first);
    foreach ($arr as $val) {
      $this->insert($val);
    }
  }

  /*
    Insert:
    - Insert by level order (i.e., from top to bottom and from left to right,
      the next available slot).
    - If parent > child, swap data. Iterate up tree.
  */
  public function insert($data) {
    $newNode = null;
    $nodes[] = $this->head;
    while ($nodes) {
      $node = array_shift($nodes);
      if (!$node->left) {
        $newNode = new HeapNode($data, $node);
        $node->left = $newNode;
        break;
      } else if (!$node->right) {
        $newNode = new HeapNode($data, $node);
        $node->right = $newNode;
        break;
      } else {
        // Append childern nodes to queue.
        $nodes = array_merge($nodes, $this->childrenNodes($node));
      }
    }

    // If parent > child, swap data.
    if ($newNode) {
      $this->tail = $newNode;
      while ($node->data > $newNode->data) {
        // Swap data.
        $tempData = $node->data;
        $node->data = $newNode->data;
        $newNode->data = $tempData;
        // Iterate up tree.
        $newNode = $node;
        $node = $node->parent;
      }
    }
  }

  /*
    Extract:
    - Return head.
    - Swap head with tail.
    - If head > any children, swap. Iterate down tree.
  */
  public function extract() {
    // Save current head data to be returned.
    $headData = $this->head->data;
    // Overwrite head data with tail data.
    $this->head->data = $this->tail->data;

    // Delete tail.
    if ($this->tail->parent->data) {
      if ($this->tail->parent->right->data == $this->tail->data) {
        $this->tail->parent->right = null;
      } else {
        $this->tail->parent->left = null;
      }
    } else {
      $this->head = null;
    }

    // If head > any children, swap. Iterate down tree.
    $node = $this->head;
    while ($node) {
      $minVal = $node->data;
      $minNode = null;
      if ($node->left->data && $minVal > $node->left->data) {
        $minVal = $node->left->data;
        $minNode = $node->left;
      }
      if ($node->right->data && $minVal > $node->right->data) {
        $minVal = $node->right->data;
        $minNode = $node->right;
      }
      if ($minNode) {
        $tempData = $node->data;
        $node->data = $minVal;
        $minNode->data = $tempData;
        $node = $minNode;
      } else {
        break;
      }
    }

    // Update tail.
    $levelOrderNodes = $this->getLevelOrderNodes();
    $this->tail = end($levelOrderNodes);

    return $headData;
  }

  public function getLevelOrderNodes() {
    $arr = [];
    $nodes = [$this->head];
    while ($nodes) {
      $node = array_shift($nodes);
      $arr[] =  $node;
      $nodes = array_merge($nodes, $this->childrenNodes($node));
    }
    return $arr;
  }

  public function getLevelOrderData() {
    $data = [];
    $nodes = $this->getLevelOrderNodes();
    foreach ($nodes as $node) {
      $data[] = $node->data;
    }
    return $data;
  }

  private function childrenNodes($node) {
    $nodes = [];
    if ($node->left) {
      $nodes[] = $node->left;
    }
    if ($node->right) {
      $nodes[] = $node->right;
    }
    return $nodes;
  }
}

class HeapNode {
  public $data;
  // Need the parent to traverse up and down the tree.
  public $parent;
  public $left;
  public $right;

  public function __construct($data, $parent=null) {
    $this->data = $data;
    if ($parent) {
      $this->parent = $parent;
    }
  }
}
