<?php

/*
  Given a list of n integers,
  write a function to determine if any number of ints sum to k.
*/

$ints = genInts(150);
$n = 50;

if ($arr = v1($n, $ints)) {
  $sumStr = implode(' + ', $arr);
  $sum = array_sum($arr);
  echo "$sumStr = $sum == $n\n";
} else {
  echo "$n\n";
  print_r($ints);
}

/*
  - First sort.
  - For each int,
    - If int >= n, exit.
    - Else for each int,
      - If int at j >= n, exit.
      - Else,
        - Create list [start_i, ..., j].
        - Sum list.
        - If sum === n, return list.
*/
function v1($n, $ints) {
  sort($ints);
  for ($i=0; $i<count($ints); $i++) {
    if ($ints[$i] >= $n) {
      break;
    }
    for ($j=1; $j<count($ints); $j++) {
      if ($ints[j] >= n) {
        break;
      }
      // My version of array_slice($ints, $i, $i+j+1)
      $slice = arraySlice($ints, $i, $i+j+1);
      // My version of array_sum($slice)
      if (arraySum($slice) === $n) {
        return $slice;
      }
    }
  }
}

// My version of array_slice.
function arraySlice($arr, $start, $end) {
  $slice = [];
  for ($i=$start; $i<=$end; $i++) {
    $slice[] = $arr[$i];
  }
  return $slice;
}

// My version of array_sum.
function arraySum($arr) {
  $sum = 0;
  foreach ($arr as $val) {
    $sum += $val;
  }
  return $sum;
}

function genInts($count) {
  $ints = [];
  for ($i=0; $i<$count; $i++) {
    $ints[] = rand(1, 100);
  }
  return $ints;
}
