<?php

/*
  Given a list of n integers,
  write a function to determine if any two sum to k.
*/

$ints = genInts(100000);
$n = genInts(1)[0];

if (list($i, $j) = v1($n, $ints)) {
  $first = $ints[$i];
  $second = $ints[$j];
  $sum = $first + $second;
  echo "[$i] [$j]\n";
  echo "$first + $second = $sum == $n\n";
} else {
  echo "$n\n";
  print_r($ints);
}

if (list($first, $second) = v2($n, $ints)) {
  $sum = $first + $second;
  echo "$first + $second = $sum == $n\n";
} else {
  echo "$n\n";
  print_r($ints);
}

/*
  Create an array.
  For each int, create an element in the array who's index is the int.
  For each int, subtract it for n. If the diff is an index in the array, return.
*/
function v2($n, $ints) {
  $keys = [];
  foreach ($ints as $int) {
    $keys[$int] = true;
  }
  foreach ($ints as $int) {
    $diff = $n - $int;
    if (isset($keys[$diff])) {
      return [$int, $diff];
    }
  }
  return null;
}

/*
  Sum pairs.
*/
function v1($n, $ints) {
  $count = count($ints);
  for ($i=0; $i<$count; $i++) {
    for ($j=$i+1; $j<$count; $j++) {
      if ($ints[$i]+$ints[$j] == $n) {
        return [$i, $j];
      }
    }
  }
  return null;
}

function genInts($count) {
  $ints = [];
  for ($i=0; $i<$count; $i++) {
    $ints[] = rand(1, 1000000);
  }
  return $ints;
}
