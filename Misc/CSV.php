<?php

$delimiter = ',';
$escapeChar = '\\';
$arr = [
  'Who',
  'are we?',
  'Someone fun!',
  '"Cmon'.$esc.', man..."', 
  'Right.',
];
$delimitedVal = implode($delimiter, $arr);
echo "$delimitedVal\n\n";

simpleExplode($delimitedVal, $delimiter);
wrapped($delimitedVal, $delimiter);
escaped($delimitedVal, $delimiter, $escapeChar);

/*
  Simply explode string by delimiter.
*/
function simpleExplode($delimitedVal, $delimiter) {
  foreach(explode($delimiter, $delimitedVal) as $val) {
    echo "$val\n";
  }
  echo "\n";
}

/*
  Cases:
  - For each char in the delimited value:
    - If current string is wrapped and current char is wrapping char,
      - Append current char.
      - Return current string.
      - Clear current string.
      - Increment index (to skip the delimiter).
    - If current string is delimiter,
      - If current string wrappped, append delimiter.
      - Otherwise if current string is not null,
        - Return current string.
        - Clear current string.
    - Otherwise,
      - Append current char.
      - If current char is wrapping char, mark current string
        as wrapped.
*/
function wrapped($delimitedVal, $delimiter) {
  $wrapped = false;
  $val = '';
  for ($i=0; $i<strlen($delimitedVal); $i++) {
    $char = $delimitedVal[$i];
    if ($wrapped && $char=='"') {
      $val .= $char;
      echo "$val\n";
      $val = '';
      $i++;
      $wrapped = false;
    } else if ($char == $delimiter) {
      if ($wrapped) {
        $val .= $char;
      } else if ($val !== '') {
        echo "$val\n";
        $val = '';
      }
    } else {
      $val .= $char;
      if ($char=='"') {
        $wrapped = true;
      }
    }
  }
  if ($val) {
    echo "$val\n";
  }
  echo "\n";
}

/*
  - For each char in delimited val:
    - If current char is escape char,
      - Increment index (to skip the escape char).
      - Append the next char.
    - If current char is delimiter,
      - Return current string.
      - Clear current string.
    - Otherwise, append current char.
*/
function escaped($delimitedVal, $delimiter, $escapeChar) {
  $val = '';
  for ($i=0; $i<strlen($delimitedVal); $i++) {
    $char = $delimitedVal[$i];
    if ($char === $escapedChar) {
      $i++;
      $val .= $delimitedVal[$i];
    } else if ($char === $delimiter) {
      echo "$val\n";
      $val = '';
    } else {
      $val .= $char;
    }
  }
  if ($val) {
    echo "$val\n";
  }
  echo "\n";
}
