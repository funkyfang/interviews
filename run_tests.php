<?php
require_once('Sorts/SortsTest.php');
require_once('DataStructures/HashTableTest.php');
require_once('DataStructures/LinkedListTest.php');
require_once('DataStructures/StackTest.php');
require_once('DataStructures/QueueTest.php');
require_once('Tests/Tests.php');

$sorts_tests = array(
  'instance' => new SortsTest(),
  'test_args' => array(
    // Test all the sorts with an array of 5000 ints.
    array(
      'algs' => ['bubble', 'fred', 'heap', 'merge', 'quick'],
      'arr_len' => 5000,
    ),
    // Compare performance of Merge and Quick sorts.
    array(
      'algs' => ['merge', 'quick'],
      'arr_len' => 20000,
    ),
  ),
);

$hashtable_tests = array(
  'instance' => new HashTableTest(15000),
  // Compare performance of hash tables with different number of buckets.
  'test_args' => array(
    10000,
    1,
  ),
);

$linkedlist_tests = array(
  'instance' => new LinkedListTest(),
  // Test appending, prepending, inserting, and deleting nodes into
  // a linked list.
  'test_args' => array(
     array(
       'func' => 'append',
       'funcArgs' => ['d'],
       'validStr' => 'd',
     ),
     array(
       'func' => 'append',
       'funcArgs' => ['e'],
       'validStr' => 'de',
     ),
     array(
       'func' => 'prepend',
       'funcArgs' => ['b'],
       'validStr' => 'bde',
     ),
     array(
       'func' => 'prepend',
       'funcArgs' => ['a'],
       'validStr' => 'abde',
     ),
     array(
       'func' => 'insertAfter',
       'funcArgs' => ['b', 'c',],
       'validStr' => 'abcde',
     ),
     array(
       'func' => 'insertAfter',
       'funcArgs' => ['y', 'z'],
       'validStr' => 'abcde',
     ),
     array(
       'func' => 'delete',
       'funcArgs' => ['a'],
       'validStr' => 'bcde',
     ),
     array(
       'func' => 'delete',
       'funcArgs' => ['e'],
       'validStr' => 'bcd',
     ),
     array(
       'func' => 'delete',
       'funcArgs' => ['c'],
       'validStr' => 'bd',
     ),
  ),
);

$stack_tests = array(
  'instance' => new StackTest(),
  'test_args' => array(
    // Test pushing and popping into a stack.
    array(
      'func' => 'push',
      'funcArgs' => ['b'],
      'validTop' => 'b',
      'validMin' => 'b',
    ),
    array(
      'func' => 'push',
      'funcArgs' => ['a'],
      'validTop' => 'a',
      'validMin' => 'a',
    ),
    array(
      'func' => 'pop',
      'funcArgs' => [],
      'validTop' => 'b',
      'validMin' => 'b',
    ),
  ),
);

$queue_tests = array(
  'instance' => new QueueTest(),
  'test_args' => array(
    // Test enqueuing and dequeuing into a queue.
    array(
      'func' => 'enqueue',
      'funcArgs' => ['a'],
      'validResult' => array(
        'first' => 'a',
        'last' => 'a',
      ),
    ),
    array(
      'func' => 'enqueue',
      'funcArgs' => ['b'],
      'validResult' => array(
        'first' => 'a',
        'last' => 'b',
      ),
    ),
    array(
      'func' => 'dequeue',
      'funcArgs' => [],
      'validResult' => array(
        'first' => 'b',
        'last' => 'b',
      ),
    ),
  ),
);

$tests = new Tests([
  $sorts_tests,
  $hashtable_tests,
  $linkedlist_tests,
  $stack_tests,
  $queue_tests,
]);
$tests->run();
