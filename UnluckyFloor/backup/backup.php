<?php

$unluckyNumber = 4;
$floorNumber = 3999;

/*
$missingFloors = 0;
for ($i=1; $i<=$floorNumber; $i++) {
  $arr = str_split($i);
  if (in_array($unluckyNumber, $arr)) {
    $missingFloors++;
  }
}
$floors = $floorNumber - $missingFloors;
print "$floorNumber = $floors + $missingFloors\n";
print "Missing floors: $missingFloors\n";
print "------\n\n";
*/

$base10 = intval(log10($floorNumber));

$base10arr = [1];
for ($i=1; $i<$base10; $i++) {
  $base10arr[] = $base10arr[$i-1]*9+pow(10, $i);
}

$randInts = [];
for ($i=0; $i<100; $i++) {
  $randInts[] = rand(0, 5000000);
}

/*
//for ($i=40; $i<=40; $i++) {
//for ($i=0; $i<=16000; $i++) {
print "$i\n";
$missing = 0;
$base10 = intval(log10($i));
$base10arr = [1];
for ($j=1; $j<$base10; $j++) {
  $base10arr[] = $base10arr[$j-1]*9+pow(10, $j);
}
//print "$i\n";
//print_r($base10arr);
$brute = brute($i);
$fred = fred($i, $base10, $base10arr, $missing);
//print "$i: " . $brute . " == $fred\n";

if ($brute !== $fred) {
  print "$i FAIL!\n\n";
  break;
}

//print "Missing floors: " . fred($floorNumber, $base10, $base10arr, $missing) . "\n";
//print brute($floorNumber) . "\n";
}
*/

for ($i=0; $i<count($randInts); $i++) {
  $floorNumber = $randInts[$i];
  print "$floorNumber\n";
  $missing = 0;
  $base10 = intval(log10($floorNumber));
  $base10arr = [1];
  for ($j=1; $j<$base10; $j++) {
    $base10arr[] = $base10arr[$j-1]*9+pow(10, $j);
  }
  $brute = brute($floorNumber);
  $fred = fred($floorNumber, $base10, $base10arr, $missing);

  if ($brute !== $fred) {
    print "$floorNumber FAIL!\n\n";
    break;
  }
}

function brute($floorNumber) {
$missingFloors = 0;
for ($i=1; $i<=$floorNumber; $i++) {
  $arr = str_split($i);
  if (in_array(4, $arr)) {
    $missingFloors++;
  }
}
return $missingFloors;
}

function fred($floorNumber, $base10, $arr, $missing) {
  if ($floorNumber < 4) {
    return $missing;
  } else if ($floorNumber >= 4 && $floorNumber < 10) {
    return $missing+1;
  }
  $base = pow(10, $base10);
  $multiplier = intval($floorNumber/$base);
/*
print "------\n";
print "$floorNumber\n";
print "$base\n";
print "$multiplier\n";
print_r($arr);
print "$missing\n";
*/
  if ($floorNumber >= (4+1)*$base) {
    $missing += $base + ($multiplier-1) * $arr[$base10-1];
    $diff = $floorNumber - ($multiplier * $base);
    $base10 = intval(log10($diff));
    if ($diff !== 0) {
      return fred($diff, $base10, $arr, $missing);
    }
    return $missing;
  } else if ($floorNumber >= 4*$base) {
    $missing += min($base, $floorNumber - 4*$base) + $multiplier * $arr[$base10-1];
    return $missing+1;
  } else {
//print "foo\n";
//print "$missing\n";
    $missing += $multiplier * $arr[$base10-1];
//print "$missing\n";
    $diff = $floorNumber - ($multiplier * $base);
    $base10 = intval(log10($diff));
    if ($diff !== 0) {
      return fred($diff, $base10, $arr, $missing);
    }
    return $missing;
  }
/*
  $base = pow(10, $base10);
  $multiplier = intval($floorNumber / $base);
  if ($floorNumber >= 4 * $base) {
    $missing += $floorNumber - 4*$base + 1;
    $missing += min(100, $floorNumber - 4*$base);
  }
  $missing += $multiplier * array_pop($arr);//$arr[$base10-1];
  $diff = $floorNumber - ($multiplier * $base);
  $base10 = intval(log10($diff));
//print "---\n";
//print "$base\n";
//print "$multiplier\n";
//print "$diff\n";
//print "$base10\n";
//print "------\n";
  if ($diff !== 0) {
    //$missing += fred($diff, $base10, $arr, $missing);
    //return $missing + fred($diff, $base10, $arr, $missing);
    return fred($diff, $base10, $arr, $missing);
  }
  return $missing;
*/
}
