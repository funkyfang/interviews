<?php
require_once('Floors.php');

$unluckyNumber = 4;
$floorHeight = 10;
$floors = new Floors($unluckyNumber, $floorHeight);

$testFloorNumbers = range(1, 600);
$randFloorNumbers = [];
for ($i=0; $i<3; $i++) {
  $randFloorNumbers[] = rand(0, 5000000);
}
$testFloorNumbers = array_merge($testFloorNumbers, $randFloorNumbers);

foreach ($testFloorNumbers as $floorNumber) {
  print "Testing $floorNumber ... ";
  $bruteCount = $floors->countMissingBruteForce($floorNumber);
  $fastCount = $floors->countMissingFast($floorNumber);

  if ($bruteCount !== $fastCount) {
    print "FAILED!\n";
    print "$bruteCount ?= $fastCount\n";
    break;
  } else {
    print "passed.\n";
  }
}
print "All tests passed.\n\n";

$randFloorNumber = rand(4000000, 5000000);
print "Floor Number: $randFloorNumber\n";
print "Height: " . $floors->numberToHeight($randFloorNumber) . "\n\n";
