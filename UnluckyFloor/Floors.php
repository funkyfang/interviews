<?php

class Floors {
  public $unluckyNumber;
  public $floorHeight;

  function __construct($unluckyNumber, $floorHeight) {
    $this->unluckyNumber = $unluckyNumber;
    $this->floorHeight = $floorHeight;
  }

  public function countMissingBruteForce($floorNumber) {
    $missingFloors = 0;
    for ($i=1; $i<=$floorNumber; $i++) {
      $arr = str_split($i);
      if (in_array($this->unluckyNumber, $arr)) {
        $missingFloors++;
      }
    }
    return $missingFloors;
  }

  public function countMissingFast($floorNumber) {
    $base10Power = intval(log10($floorNumber));
    $missingFloorsPerBase10 = [1];
    for ($i=1; $i<$base10Power; $i++) {
      $missingFloorsPerBase10[] = $missingFloorsPerBase10[$i-1]*9 +
        pow(10, $i);
    }
    $missingFloors = 0;
    return $this->countMissing($floorNumber, $base10Power,
      $missingFloorsPerBase10, $missingFloors);
  }

  private function countMissing($floorNumber, $base10Power,
    $missingFloorsPerBase10, $missingFloors) {

    if ($floorNumber < $this->unluckyNumber) {
      return $missingFloors;
    } else if ($floorNumber < 10) {
      return $missingFloors+1;
    }

    $base = pow(10, $base10Power);
    $multiplier = intval($floorNumber/$base);
    if (($floorNumber >= $this->unluckyNumber*$base) &&
        ($floorNumber < ($this->unluckyNumber+1)*$base)) {
      $missingFloors += $floorNumber - $this->unluckyNumber*$base +
        $multiplier*$missingFloorsPerBase10[$base10Power-1];
      return $missingFloors+1;
    } else {
      $missingFloors += ($floorNumber >= ($this->unluckyNumber+1)*$base) ?
        $base + ($multiplier-1) * $missingFloorsPerBase10[$base10Power-1] :
        $multiplier * $missingFloorsPerBase10[$base10Power-1];
      $remainingFloors = $floorNumber - $multiplier*$base;
      $remainingBase10Power = intval(log10($remainingFloors));
      if ($remainingFloors !== 0) {
        return $this->countMissing($remainingFloors, $remainingBase10Power,
          $missingFloorsPerBase10, $missingFloors);
      }
      return $missingFloors;
    }
  }

  public function numberToHeight($floorNumber) {
    return $this->floorHeight * ($floorNumber-$this->countMissingFast($floorNumber));
  }
}
